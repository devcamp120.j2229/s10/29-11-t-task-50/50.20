package com.devcamp.j50_javabasic.s10;

public class NewDevcampApp {
    public static void main(String[] args) {
        System.out.println("New Devcamp app");

        NewDevcampApp app = new NewDevcampApp();
        app.printName();
        System.out.println("Return method: " + app.name());

        NewDevcampApp.printStaticName();
    }

    // Hàm trả ra giá trị
    public String name() {
        String name = "String Return";

        return name;
    }

    // Hàm thực thi (không trả ra giá trị)
    public void printName() {
        System.out.println("Print String");
    }

    // Hàm thực thi static (không trả ra giá trị)
    public static void printStaticName() {
        System.out.println("Print String");
    }

}
