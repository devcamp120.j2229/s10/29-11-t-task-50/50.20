import com.devcamp.j50_javabasic.s10.NewDevcampApp;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        NewDevcampApp.printStaticName();

        NewDevcampApp newApp = new NewDevcampApp();
        System.out.println("Return method: " + newApp.name());
        newApp.printName();
    }
}
